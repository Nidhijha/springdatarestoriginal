package com.ust.rest.service;

import com.ust.rest.entity.Student;

public interface StudentService {
	
	Student findById(Integer id);

	Student register(Student stud);
}
