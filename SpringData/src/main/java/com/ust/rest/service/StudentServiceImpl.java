package com.ust.rest.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ust.rest.dao.StudentDao;
import com.ust.rest.entity.Student;
import com.ust.rest.exception.StudentException;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentDao sDao;
	
	@Override
	public Student findById(Integer id)  {
		Optional<Student> optional = sDao.findById(id);
		if(!optional.isPresent()) {
			System.out.println("in sdao empty optional ");
			throw new StudentException("Student Not found for id:"+id);
		}
		Student stud = optional.get();
		return stud;
	}

	@Override
	public Student register(Student stud) {
		boolean exists = stud.getId()!=null && sDao.existsById(stud.getId());
	      if(exists) {
	         throw new StudentException("Student already exists for id:"+stud.getId());
	      }
	      Student regStud = sDao.save(stud);
	      return regStud;
	}

}
