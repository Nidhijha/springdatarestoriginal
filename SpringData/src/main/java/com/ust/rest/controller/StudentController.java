package com.ust.rest.controller;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ust.rest.dto.CreateStudentRequest;
import com.ust.rest.dto.StudentDetails;
import com.ust.rest.entity.Course;
import com.ust.rest.entity.Student;
import com.ust.rest.service.StudentService;
import com.ust.rest.util.StudentUtil;

@RestController
@RequestMapping("/student")
@Validated
public class StudentController {

	@Autowired
	private StudentUtil studentUtil; // for converting Student entity to StudentDetails dto
	// [ dto : data transfer object]
	
	private static final Logger logger = LoggerFactory.getLogger(StudentController.class);
	
	@Autowired
	private StudentService sService;
	
	//SELECT
	@GetMapping("/by/id/{id}")
	public StudentDetails fetchStudent(@PathVariable("id") Integer id) {
		logger.info("Inside fetch controller id:"+id);
		System.out.println("find by id:"+id); //115
//		Student student = new Student(1234, "Utkarsh", "Gupta", 21, null);
//		Course course = new Course(2233, "Java", 5000, student);
//		Set<Course> courses = new HashSet<>();
//		courses.add(course);
//		student.setCourses(courses);
		Student student = sService.findById(id);
		StudentDetails details = studentUtil.toDetails(student);
		return details;
	}
	
	//INSERT
	@PostMapping("/add")
	public StudentDetails addStudent(@RequestBody @Valid CreateStudentRequest requestData) {
	      System.out.println("req data: " + requestData);
	      Student stud = studentUtil.toStudent(requestData);
	      System.out.println("stud: " + stud);
	      Student regStud = sService.register(stud);
	      StudentDetails details = studentUtil.toDetails(regStud);
	      return details;
	   }
}
